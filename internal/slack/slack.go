package slack

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/slack-go/slack"
	"gitlab.com/btcdirect-api/prslackbot/internal/gitlab"
)

var emptyMessages = [8]string{
	"Good work y'all, there are no open PRs! 🔥 Let's have a cheat day... 🍕",
	"You've squashed those PRs like sets in the gym. 🔥 Keep up the killer work, team! 🤜🤛",
	"There are no open PRs, let's break out the virtual protein shakes! 🍾 Don't worry, we won't tell HR. 😉",
	"No open PRs left! 🔥 Now, onto the next challenge: deciding which emoji best represents our coding victory. ⤵️",
	"Closing PRs like it's leg day at the gym! 🔥 We're building code muscles one commit at a time. 💪",
	"No open PRs? That's what I call a coding marathon! 🏃🔥 Now, let's hydrate, stretch, and get ready for the next batch. 💪",
	"There are no open PRs! 🔥 You've sprinted through them like Usain Bolt on steroids! 💊",
	"You crushed those PRs in a new PR! ⏱️ Time for a well-deserved cooldown... 🔥 ",
}

func SendMessage(channel, label string, prs []gitlab.Pr) {
	api := slack.New(os.Getenv("SLACK_ACCESS_TOKEN"))
	sendBlocks(api, channel, buildHeaderBlocks(prs))
	for _, pr := range prs {
		sendBlocks(api, channel, buildPrBlocks(pr, label))
	}
	sendBlocks(api, channel, buildFooterBlocks(label))
}

func sendBlocks(api *slack.Client, channel string, blocks []slack.Block) {
	_, _, err := api.PostMessage(channel, slack.MsgOptionBlocks(blocks...))
	if err != nil {
		log.Fatalf("Failed to send slack message: %v", err)
	}
}

func buildHeaderBlocks(prs []gitlab.Pr) []slack.Block {
	initMessage := "📢 *Let's 🔥BURN🔥 those story points! No excuses! 🏋️*"
	if len(prs) == 0 {
		r := rand.New(rand.NewSource(time.Now().Unix()))
		emptyMessage := emptyMessages[r.Intn(len(emptyMessages))]
		initMessage = "📢 *" + emptyMessage + "*"
	}
	return []slack.Block{
		slack.NewSectionBlock(&slack.TextBlockObject{
			Type: slack.MarkdownType,
			Text: initMessage,
		}, nil, nil),
	}
}

func buildFooterBlocks(label string) []slack.Block {
	blocks := make([]slack.Block, 0, 2)

	blocks = addDividerBlock(blocks)
	blocks = append(blocks, slack.NewSectionBlock(&slack.TextBlockObject{
		Type: slack.MarkdownType,
		Text: fmt.Sprintf("_Want to burn your points? 🔥 Label your PRs with `%s` to be included in here._", label),
	}, nil, nil))

	return blocks
}

func buildPrBlocks(pr gitlab.Pr, label string) []slack.Block {
	blocks := make([]slack.Block, 0, 10)

	blocks = addDividerBlock(blocks)
	blocks = addPrBlock(blocks, pr)

	return blocks
}

func addDividerBlock(blocks []slack.Block) []slack.Block {
	return append(blocks, slack.NewDividerBlock())
}

func addPrBlock(blocks []slack.Block, pr gitlab.Pr) []slack.Block {
	blocks = append(blocks, slack.NewSectionBlock(&slack.TextBlockObject{
		Type: slack.MarkdownType,
		Text: fmt.Sprintf(":fox_face: *<%s|%s>*", pr.URL, pr.Title),
	}, nil, nil))

	pipelineEmoji := "🔴"
	if pr.HasConflicts {
		pipelineEmoji = "⚠️"
	} else if pr.PipelineSucceeded {
		pipelineEmoji = "🟢"
	}
	project := pipelineEmoji + " " + pr.Project
	blocks = append(blocks, slack.NewContextBlock("",
		slack.NewTextBlockObject(slack.MarkdownType, "*Project:* ", false, false),
		slack.NewTextBlockObject(slack.MarkdownType, project, false, false),
	))

	diff := time.Since(*pr.CreatedAt)
	days := (int(diff.Hours()) + 12) / 24
	daysString := "days"
	if days == 1 {
		daysString = "day"
	}

	blocks = append(blocks, slack.NewContextBlock("",
		slack.NewTextBlockObject(slack.MarkdownType, "*Created by:* ", false, false),
		slack.NewImageBlockElement(pr.CreatedBy.AvatarURL, pr.CreatedBy.Name),
		slack.NewTextBlockObject(slack.MarkdownType, pr.CreatedAt.Format("02 Jan 2006 15:04"), false, false),
		slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf("(*%d %s ago*)", days, daysString), false, false),
	))

	reviewerBlocks := []slack.MixedElement{
		slack.NewTextBlockObject(slack.MarkdownType, "*Reviewers:*", false, false),
	}

	for _, reviewer := range pr.Reviewers {
		emoji := ":x:"
		if reviewer.Approved {
			emoji = ":white_check_mark:"
		} else if reviewer.HasOpenThread {
			emoji = ":speech_balloon:"
		} else if reviewer.HasClosedThread {
			emoji = "🔁"
		}

		reviewerBlocks = append(reviewerBlocks,
			slack.NewImageBlockElement(reviewer.AvatarURL, reviewer.Name),
			slack.NewTextBlockObject(slack.MarkdownType, emoji, false, false),
		)
	}

	for i := len(pr.Reviewers); i < pr.RequiredReviewers; i++ {
		reviewerBlocks = append(reviewerBlocks, slack.NewTextBlockObject(slack.MarkdownType, ":hourglass:", false, false))
	}

	blocks = append(blocks, slack.NewContextBlock("", reviewerBlocks...))

	return blocks
}
