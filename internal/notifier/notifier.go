package notifier

import (
	"fmt"

	"gitlab.com/btcdirect-api/prslackbot/internal/gitlab"
	"gitlab.com/btcdirect-api/prslackbot/internal/slack"
)

func Notify(slackChannel, gitlabLabel string) {
	fmt.Printf("Notifying slack channel %s about open PRs with label %s\n", slackChannel, gitlabLabel)
	fmt.Printf("Retrieving open PRs with label %s\n", gitlabLabel)

	prs := gitlab.GetOpenPrs(gitlabLabel)
	fmt.Printf("Found %d open PRs with label %s\n", len(prs), gitlabLabel)

	fmt.Printf("Sending slack notification to channel %s\n", slackChannel)
	slack.SendMessage(slackChannel, gitlabLabel, prs)

	fmt.Println("PRSlackBot is done for the day, bye!")
}
