# PRSlackBot

## Environment variables
- `SLACK_ACCESS_TOKEN`
- `GITLAB_ACCESS_TOKEN`

## Executing the command
```bash
go run cmd/prslackbot/prslackbot SLACK_CHANNEL GITLAB_LABEL
```

`SLACK_CHANNEL` represents the channel to which the notification will be sent (doh).

`GITLAB_LABEL` represents the label in Gitlab that will be filtered on, for example `Backend` or `Sprint Goal`