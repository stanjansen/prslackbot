package main

import (
	"log"
	"os"

	"gitlab.com/btcdirect-api/prslackbot/internal/notifier"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatal("Missing arguments, usage: prslackbot <slack-channel> <gitlab-label>")
	}

	slackChannel := os.Args[1]
	gitlabLabel := os.Args[2]
	notifier.Notify(slackChannel, gitlabLabel)
}
