package gitlab

import (
	"log"
	"os"
	"slices"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
)

const (
	DEFAULT_REQUIRED_APPROVALS  = 2
	FRONTEND_REQUIRED_APPROVALS = 1
)

type Pr struct {
	Title             string
	URL               string
	Project           string
	CreatedBy         *User
	Reviewers         []Reviewer
	RequiredReviewers int
	PipelineSucceeded bool
	CreatedAt         *time.Time
	HasConflicts      bool
}

type User struct {
	Name      string
	AvatarURL string
}

type Reviewer struct {
	User
	Approved        bool
	HasOpenThread   bool
	HasClosedThread bool
}

func GetOpenPrs(label string) []Pr {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_ACCESS_TOKEN"))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	mrs, _, err := git.MergeRequests.ListGroupMergeRequests("btcdirect-api", &gitlab.ListGroupMergeRequestsOptions{
		Labels: &gitlab.LabelOptions{label},
		WIP:    gitlab.Ptr("no"),
		State:  gitlab.Ptr("opened"),
	})
	if err != nil {
		log.Fatalf("Failed to list merge requests: %v", err)
	}

	if len(mrs) == 0 {
		return []Pr{}
	}

	prs := make([]Pr, 0, len(mrs))
	for _, mr := range mrs {
		prs = append(prs, createPr(mr, git))
	}
	return prs
}

func createPr(mr *gitlab.MergeRequest, git *gitlab.Client) Pr {
	pipelines, _, err := git.MergeRequests.ListMergeRequestPipelines(mr.ProjectID, mr.IID)
	if err != nil {
		log.Fatalf("Failed to get pipelines: %v", err)
	}

	pipelineSucceeded := false
	if len(pipelines) > 0 {
		pipelineSucceeded = pipelines[0].Status == "success"
	}

	requiredApprovals := DEFAULT_REQUIRED_APPROVALS
	for _, label := range mr.Labels {
		if label == "Frontend" {
			requiredApprovals = FRONTEND_REQUIRED_APPROVALS
		}
	}

	var u *User
	if mr.Author != nil {
		u = &User{Name: mr.Author.Name, AvatarURL: mr.Author.AvatarURL}
	}

	return Pr{
		Title:             mr.Title,
		URL:               mr.WebURL,
		Project:           strings.Split(strings.Split(mr.References.Full, "/")[1], "!")[0],
		CreatedBy:         u,
		Reviewers:         createReviewers(mr, git),
		RequiredReviewers: requiredApprovals,
		PipelineSucceeded: pipelineSucceeded,
		CreatedAt:         mr.CreatedAt,
		HasConflicts:      mr.HasConflicts,
	}
}

func createReviewers(mr *gitlab.MergeRequest, git *gitlab.Client) []Reviewer {
	reviewers := make([]Reviewer, 0, len(mr.Reviewers))
	if len(mr.Reviewers) == 0 {
		return reviewers
	}

	approvals, _, err := git.MergeRequestApprovals.GetApprovalState(mr.ProjectID, mr.IID)
	if err != nil {
		log.Fatalf("Failed to get approvals: %v", err)
	}

	comments, _, err := git.Discussions.ListMergeRequestDiscussions(mr.ProjectID, mr.IID, &gitlab.ListMergeRequestDiscussionsOptions{})
	if err != nil {
		log.Fatalf("Failed to get discussions: %v", err)
	}

	approverIds := make([]int, 0, len(reviewers))
	for _, rule := range approvals.Rules {
		for _, approver := range rule.ApprovedBy {
			approverIds = append(approverIds, approver.ID)
		}
	}

	for _, reviewer := range mr.Reviewers {
		hasOpenThread := false
		hasClosedThread := false
		for _, comment := range comments {
			for _, note := range comment.Notes {
				if note.Author.ID == reviewer.ID {
					if note.Resolved {
						hasClosedThread = true
					} else if note.Resolvable {
						hasOpenThread = true
					}
				}
			}
		}
		reviewers = append(reviewers, Reviewer{
			User:            User{Name: reviewer.Name, AvatarURL: reviewer.AvatarURL},
			Approved:        slices.Contains(approverIds, reviewer.ID),
			HasOpenThread:   hasOpenThread,
			HasClosedThread: hasClosedThread,
		})
	}
	return reviewers
}
